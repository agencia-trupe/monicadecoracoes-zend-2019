<?php

class PortfolioController extends Zend_Controller_Action
{

    public function init()
    {
        /* models */
        $this->categorias = new Application_Model_Db_Categorias();
        $this->produtos = new Application_Model_Db_Produtos();
        // $this->produtos_categorias = new Application_Model_Db_ProdutosCategorias();
        // $this->destaques = new Application_Model_Db_Destaques();
        
        $this->view->meta_description = 'Portfólio '.SITE_TITLE;
    }

    public function indexAction()
    {
        $order = 'data_cad desc';
        $where = null;

        $cat_alias = ($this->_hasParam('categoria')) ? $this->_getParam('categoria') : null;
        $this->view->cat_alias = $cat_alias;
        
        $categs = _utfRows($this->categorias->fetchAll(
        	'status_id=1',
        	array('categoria_id','ordem')
        ));
        // _d($categs);

        $categoria = null; $subcategoria = null;
        $categorias = array(); $subcategorias = array();
        foreach($categs as $cat) {
        	if($cat_alias==$cat->alias) $categoria = $cat;
        	
        	if(!$cat->categoria_id){
        		$categorias[$cat->id] = $cat;
        		continue;
        	}

        	if(!array_key_exists($cat->categoria_id, $subcategorias))
        		$subcategorias[$cat->categoria_id] = array();
    		$subcategorias[$cat->categoria_id][] = $cat;
        	if($cat_alias==$cat->alias) $subcategoria = $cat;
        }
        if(!$cat_alias) $categoria = reset($categorias);
        	else if($subcategoria) $categoria = $categorias[$subcategoria->categoria_id];
        // if($cat_alias) $subcategoria = reset(@$subcategorias[$categoria->id]);
        
        $subcategorias = @$subcategorias[$categoria->id];

        $this->view->categoria = $categoria; //_d($categoria);
        $this->view->categorias = $categorias; //_d($categorias);
        $this->view->subcategoria = $subcategoria; //_d($subcategoria);
        $this->view->subcategorias = $subcategorias; //_d($subcategorias);

        $where_pcat = null; $where_pcat_id = null; $where_pids = 'and id in (0)';
        if($cat_alias) {
        	$cat_id = $subcategoria ? $subcategoria->id : $categoria->id;
        	if(!$subcategoria) {
        		$ccats = $this->categorias->s('categorias','id','categoria_id = "'.$categoria->id.'"');
        		$ccats_ids = array_map(function($ccat){
        			return $ccat->id;
        		}, $ccats);
        		$cat_id = ($ccats_ids) ? implode('","', $ccats_ids) : 0;
        	}

        	$where_pcat = 'categoria_id in ("'.$categoria->id.'","'.$cat_id.'")';
        	// _d($where_pcat);
        	$pcats = $this->categorias->s('produtos_categorias','*',$where_pcat);
        	if($pcats) {
        		$where_pids = array();
        		foreach ($pcats as $pcat) $where_pids[] = $pcat->produto_id;
        		$where_pids = 'and id in ("'.implode('","', $where_pids).'") ';
        	}
        } else {
        	$where_pids = '';
        }
        // _d($where_pids);

        $rows = $this->categorias->q(
        	'select p.*, pc.categoria_id cat_id from produtos p '.
        	'left join produtos_categorias pc on pc.produto_id = p.id '.
        	'where 1=1 '.
        	'and p.status_id = 1 '.$where_pids.' '.
        	'order by data_cad desc '
        );
        $this->produtos->getFotos($rows,1);
        $this->view->rows = $rows;
        // _d($rows);

    }


}

