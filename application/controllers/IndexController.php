<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        // $this->pacotes = db_table('pacotes');
        $this->categorias = db_table('categorias');
        $this->banners = db_table('destaques');
    }

    public function indexAction()
    {
        $banners = $this->banners->fetchAllWithPhoto('status_id=1','ordem');
        $this->view->banners = $banners;
        // _d($banners);

        // $pacotes = $this->pacotes->fetchAllWithFoto('status_id=1','titulo',null,null,array(
        //     'group' => 't1.id'
        // ));
        // $this->view->pacotes = $pacotes;
        // // _d($pacotes);

        $categorias = $this->categorias->fetchAllWithFoto('status_id=1 and categoria_id is null','ordem',null,null,array(
        	'group' => 't1.id'
        ));
        $this->view->categorias = $categorias;
        // _d($categorias);
    }


}