<?php

class OrcamentoController extends ZendPlugin_Controller_Action
{
	public $pagina_id = 3;

    public function init()
    {
        $this->pacotes = db_table('pacotes');
        $this->paginas = db_table('paginas');
    }

    public function indexAction()
    {
        $pagina = $this->paginas->getPagina($this->pagina_id);
        $this->view->pagina = $pagina;
        // _d($pagina);

        $pacotes = $this->pacotes->fetchAllWithFoto('status_id=1','titulo',null,null,array(
        	'group' => 't1.id'
        ));
        $pacotes = $this->pacotes->parseArquivos($pacotes);
        $this->view->pacotes = $pacotes;
        // _d($pacotes);
    }


}

