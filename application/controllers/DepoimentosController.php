<?php

class DepoimentosController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->depoimentos = db_table('depoimentos');
    }

    public function indexAction()
    {
        // paginacao
        $limit = ENV_DEV ? 1 : 6;
        $paginacao = $this->pagination($limit,10,$this->depoimentos->count('status_id = 1'));
        // _d($paginacao);

        $rows = $this->depoimentos->fetchAllWithPhoto('t1.status_id=1','ordem',$limit,$paginacao->offset);
        $this->view->rows = $rows;
        // _d($rows);

        // $paginacao->total = 100;
        $this->view->pagination = $paginacao;

        if($this->isAjax()) {
            foreach ($rows as $row) {
                $row->data = dtbr($row->data);
                $row->body = nl2br($row->body);
            }

            exit(json_encode((bool)$rows ?
                (object)array('rows'=>$rows) :
                (object)array('error'=>'Nenhum registro encontrado')
            ));
        }
    }


}

