<?php

class VideosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->videos = db_table('videos');
    }

    public function indexAction()
    {
        $rows = _utfRows($this->videos->fetchAll(
        	'status_id = 1',
        	'ordem desc' // 'ordem'
        ));
        $this->view->rows = $rows;
        // _d($rows);
    }


}

