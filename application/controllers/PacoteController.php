<?php

class PacoteController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->pacotes = db_table('pacotes');
    }

    public function indexAction()
    {
    	list($id,$alias) = paramAliasId($this->_getParam('alias'));
    	if(!$alias) return $this->_redirect('orcamento');
        // _d(array($id,$alias));

        $row = _utfRow($this->pacotes->get($id));
        $row->fotos = $this->pacotes->getFotosById($id);
        $this->view->row = $row;
        // _d($row);
    }


}

