<?php

class Application_Model_Db_ProdutoFormato extends Zend_Db_Table
{
    protected $_name = "produtos_formatos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Produtos',
        'Application_Model_Db_Formatos'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Produtos' => array(
            'columns' => 'produto_id',
            'refTableClass' => 'Application_Model_Db_Produtos',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_Formatos' => array(
            'columns' => 'formato_id',
            'refTableClass' => 'Application_Model_Db_Formatos',
            'refColumns'    => 'id'
        )
    );
}
