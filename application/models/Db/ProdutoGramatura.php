<?php

class Application_Model_Db_Produtogramatura extends Zend_Db_Table
{
    protected $_name = "produtos_gramaturas";
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Produtos',
        'Application_Model_Db_Gramaturas'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Produtos' => array(
            'columns' => 'produto_id',
            'refTableClass' => 'Application_Model_Db_Produtos',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_Gramaturas' => array(
            'columns' => 'gramatura_id',
            'refTableClass' => 'Application_Model_Db_Gramaturas',
            'refColumns'    => 'id'
        )
    );
}
