<?php

class Application_Model_Db_PacotesFotos extends Zend_Db_Table
{
    protected $_name = "pacotes_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Pacotes');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pacotes' => array(
            'columns' => 'pacote_id',
            'refTableClass' => 'Application_Model_Db_Pacotes',
            'refColumns'    => 'id'
        )
    );
}
