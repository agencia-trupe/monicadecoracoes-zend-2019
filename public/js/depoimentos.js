console.log('deps');

var last_i = 0;//Number($('#deps .dep').last().data('i'));

$('.load-deps').click(function(e){
	e.preventDefault();

	var $this = $(this),
		last_page = $this.data('page'), lastdep, 
		url = [URL,CONTROLLER,'index.json?page='+last_page].join('/'),
		i = (last_i==2) ? 0 : last_i, 
		row, j, html='', cssclass='', css='', img='';

	// console.log(url); return;
	
	$.getJSON(url,function(json){
		if(json.error){
			alert(json.error);
			return false;
		}
		// console.log(json.rows); return;

		for(j in json.rows){
			i++;
			row = json.rows[j];
			lastdep = $('#deps .dep').last();
			// console.log(row); continue;

			if(i==1) cssclass = lastdep.hasClass('dep-l') ? 'dep-r' : 'dep-l';
			if(i==2) cssclass = lastdep.hasClass('dep-l') ? 'dep-l' : 'dep-r';
			// console.log([i,cssclass]); continue;
			img = row.foto_path.length ? IMG_URL+'/depoimentos/'+row.foto_path : 0;
			css = 'style=background-image:url('+img+')';

			html+= '<div class="dep '+cssclass+'" data-i="'+i+'">';
			if(img) html+= '<figure class="img"><img src="'+img+'" alt="'+row.nome+'"></figure>';
			html+= '<div class="body">';
			html+= '<p class="text">'+row.body+'</p>';
			html+= '<div class="data-nome">';
			html+= '<span class="data">'+row.data+'</span>';
			html+= '<span class="nome">'+row.nome+'</span>';
			html+= '</div>';
			html+= '</div>';
			html+= '</div>';

			if(i==2) i = 0;
		}

		$('#deps-load-area').append(html);
		$this.data('page',Number(last_page)+1);
	});
	
	return false;
});