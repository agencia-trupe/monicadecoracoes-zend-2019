if($('#slideshow .foto').size()>1) _loadCycle(function(){
	// return;
	$('#slideshow').before('<ul id="banner-nav">').cycle({
		slideResize: true,
		containerResize: true,
		width: '100%',
		height: '100%',
		fit: 1,
		
		fx:'fade',
		speed:2000,
		// timeout: 0,
		// speedIn:4000,
		// speedOut:100,
		pager:"#banner-nav"

		,after: function(currSlideElement, nextSlideElement, options, forwardFlag) {
            // console.log('after');
            // console.log([currSlideElement, nextSlideElement, options, forwardFlag]);
            // $('#slideshow .foto').removeClass('active');
            // $(nextSlideElement).addClass('active');
        }
        ,onPagerEvent: function(zeroBasedSlideIndex, slideElement) {
            // console.log('onPagerEvent');
            // console.log([zeroBasedSlideIndex, slideElement]);
        }
        ,onPrevNextEvent: function(isNext, zeroBasedSlideIndex, slideElement) {
            // console.log('onPrevNextEvent');
            // console.log([isNext, zeroBasedSlideIndex, slideElement]);
        }
	});
	$('#slideshow .desc').show();
	_sto(function() {
        $('#slideshow .foto').addClass('active');
	},.2);
});
