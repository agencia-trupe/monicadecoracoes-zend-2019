var $rows = $('.produto:not(.hidden)');

$('.scat').click(function(e){
	e.preventDefault();

	var $this = $(this),
		catid = $this.data('catid'),
		url   = $this.attr('href'),
		hash  = '#cat-'+catid,
		hash1 = url;

	// $rows.filter(':not(.cat-'+catid+')').fadeOut();
	$rows.fadeOut('fast',function(){
		if(catid=='all') $rows.fadeIn('slow');
		else $rows.filter('.cat-'+catid).fadeIn('slow');
	});

	var filterType = 'catapl', psData = {};
		psData[filterType] = hash;
	_push(psData,filterType,hash1);

	$('.scat').removeClass('active');
	$this.addClass('active');

	return false;
});