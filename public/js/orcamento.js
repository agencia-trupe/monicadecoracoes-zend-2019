$(document).ready(function(){
    _prevalues();
    
    $('input#telefone').focus(function(){
        $(this).removeClass('pre');
    }).blur(function(){
        var $t = $(this);
        if($t.val() == ''){
            $t.addClass('pre');
        }
    });
    
    // enviando orcamento
    $('a#submit').click(function(){
        var $status = $('p.status').removeClass('error');
        $status.html('Enviando...');
        
        head.js(JS_PATH+'/is.simple.validate.js',function(){
            if($('form.orcamento').isSimpleValidate({pre:true})){
                var url  = URL+'/'+CONTROLLER+'/enviar.json',
                    data = $('form.orcamento').serialize();
                
                $.post(url,data,function(json){
                    if(json.error){
                        $status.addClass('error').html(json.error);
                        return false;
                    }
                    
                    // $status.html(json.msg);
                    $status.html(json.msg);
                    _resetForm('form.orcamento');
                    /*Shop.messageBox('<p class="shop-message-title orcamento">'+json.msg+'</p>'+
                                    '<p class="shop-message-buttons">'+
                                    '<a href="#" class="shop-close-message-box fechar-orcamento single">Fechar</a>'+
                                    '</p>');*/
                },'json');
            } else {
                $status.addClass('error').html('* Preencha todos os campos');
            }
        });
    });
});

function _prevalues(v){
    $('.prevalue').each(function(){
        var $t = $(this);
        $t.prevalue(v || $t.data('prevalue'));
    });
}

function _resetForm(formSelector){
    $(formSelector).find('input,textarea').val('');
    $(formSelector).find('select option:first-child').attr('selected',true);
}